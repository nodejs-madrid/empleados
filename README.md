1. Crea una carpeta vacía para el proyecto y genera un archivo package.json.
2. Crea un archivo de entrada llamado index.js.
3. A nuestro programa se le puede llamar pasándole un parámetro opcional. El parámetro es --full (abreviatura -f). Este parámetro sirve para que el usuario nos diga si quiere que imprimamos todos los datos o sólo algunos.
4. Si no se le ha pasado el parámetro, nuestro programa preguntará (con una pregunta de sí o no) si quiere imprimir todos los datos o no.
5. Una vez recogida la opción del usuario, hacemos una petición GET a http://dummy.restapiexample.com/api/v1/employees.
6. Dependiendo de la opción que ha elegido el usuario, imprimiremos por pantalla:
    - Número total de empleados.
    - Edad media de los empleados.
    - Salario medio de los empleados.
    - Si se ha elegido la opción "Todos los datos", imprimir también la lista de todos los empleados con nombre, edad y salario (la imagen no).
